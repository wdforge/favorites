<?php

/**
 * Microservice
 * 
 * @package    Favorites
 * @version    1.0
 */
// серверные и сервисные рутовые конфиги
if(!defined("ROOT_SERVER_CONFIG")) {
	define('ROOT_SERVER_CONFIG', dirname(__FILE__) . '/../../server.cfg.php');
}

if(!defined("ROOT_SERVICES_CONFIG")) {
	define('ROOT_SERVICES_CONFIG', dirname(__FILE__) . '/../../services.cfg.php');
}

// автозагрузчик классов
require_once(dirname(__FILE__) . '/../../modules/System/Class/AutoLoader.php');

// инициализация конфигов
$loader = new Class_AutoLoader([

    ROOT_SERVER_CONFIG,
    ROOT_SERVICES_CONFIG,
    // сервисный конфиг 
    dirname(__FILE__) . '/config.php',
    ]);

// инициализация загрузчика классов
$loader->register();

try {

    $service = new Favorites_Service();
    $service->run();
} catch (Exception $e) {
    throw $e;
}
