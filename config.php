<?php

/**
 * Microservice
 * 
 * @package    Favorites
 * @version    1.0
 */

global $dbPdo, $dbYmarket, $solrYmarket, $solr;

if(!defined("ROOT_SERVER_CONFIG")) {
	define('ROOT_SERVER_CONFIG', dirname(__FILE__) . '/../../server.cfg.php');
}

if(!defined("ROOT_SERVICES_CONFIG")) {
	define('ROOT_SERVICES_CONFIG', dirname(__FILE__) . '/../../services.cfg.php');
}

return [

    // пути для автозагрузки классов
    'autoload_paths' => [
        // локальные пути сервиса
        realpath(dirname(__FILE__) . '/src/'),
    ],

    // роутинг сразу на заданный контроллер
	'primary_controller' => [
	    'favorites' => 'Favorites_Controller'
    ],

    // настройка для инициализации репозитория и его получение через ServiceManager
    'repositories' => [
        'Item_Favorite' => [
            'class' => 'Favorites_Repository',
            'params' => [
                'connect' => $dbPdo,
                'table' => 'favorites',
                'field_id' => 'favorite_id',
            ],
        ],
    ],
];
