<?php

/**
 * Microservice
 * 
 * @package    Favorites/Controller
 * @version    1.0
 */

class Favorites_Controller extends Action_Controller {
    /**
     * @var Favorites_Repository
     */
    private $repository;
    protected $_requestFilter = [

            'indexAction' => [
                '__isRequest' => true
            ],

            'isFavoritesAction' => [

            '__isRequest' => false,

            'guid' => [
                'from' => 'GET',
                'type' => 'string',
                'regexp' => '/^[a-z0-9]+$/',
                'error' => 'Request param "guid" is not valid set.',
            ],

            'offer_id' => [
                'from' => 'GET',
                'type' => 'integer',
                'regexp' => '/^[0-9]+$/',
                'error' => 'Request param "offer_id" is not valid set.',
            ],

            'domain' => [
                'from' => 'GET',
                'type' => 'string',
                'default' => null,
                'regexp' => '/^[a-z0-9\.\-]+$/',
                'error' => 'Request param "domain" is not valid set.',
            ],

            'offer_type' => [
                'from' => 'GET',
                'type' => 'string',
                'default' => null,
                'regexp' => '/^[a-z0-9\.\-]+$/',
                'error' => 'Request param "offer_type" is not valid set.',
            ],

        ],

        'removeFavoritesAction' => [

            '__isRequest' => false,

            'guid' => [
                'from' => 'GET',
                'type' => 'string',
                'regexp' => '/^[a-z0-9]+$/',
                'error' => 'Request param "guid" is not valid set.',
            ],
            'offer_id' => [
                'from' => 'GET',
                'type' => 'integer',
                'regexp' => '/^[0-9]+$/',
                'error' => 'Request param "offer_id" is not valid set.',
            ],

            'domain' => [
                'from' => 'GET',
                'type' => 'string',
                'default' => null,
                'regexp' => '/^[a-z0-9\.\-]+$/',
                'error' => 'Request param "domain" is not valid set.',
            ],

            'offer_type' => [
                'from' => 'GET',
                'type' => 'string',
                'default' => null,
                'regexp' => '/^[a-z0-9\.\-]+$/',
                'error' => 'Request param "offer_type" is not valid set.',
            ],
        ],

        'addFavoritesAction' => [

            '__isRequest' => false,

            /* GET */
            'guid' => [
                'from' => 'GET',
                'type' => 'string',
                'regexp' => '/^[a-z0-9]+$/',
                'error' => 'Request param "guid" is not valid set.',
            ],

            'offer_id' => [
                'from' => 'GET',
                'type' => 'integer',
                'regexp' => '/^[a-z\.\-0-9]+$/',
                'error' => 'Request param "offer_id" is not valid set.',
            ],

            'domain' => [
                'from' => 'GET',
                'type' => 'string',
                'default' => null,
                'regexp' => '/^[a-z\.\-0-9]+$/',
                'error' => 'Request param "site" is not valid set.',
            ],

            'offer_type' => [
                'from' => 'GET',
                'type' => 'integer',
                'default' => null,
                'regexp' => '/^[a-z\.\-0-9]+$/',
                'error' => 'Request param "offer_type" is not valid set.',
            ],
        ],

        'addFavoritesFromSessionAction' => [
            '__isRequest' => false,

            /* GET */
            'guid' => [
                'from' => 'GET',
                'type' => 'string',
                'regexp' => '/^[a-z0-9]+$/',
                'error' => 'Request param "guid" is not valid set.',
            ],

            'offers' => [
                'from' => 'GET',
                'error' => 'Request param "offers" is not valid set.',
            ],

            'domain' => [
                'from' => 'GET',
                'type' => 'string',
                'default' => null,
                'regexp' => '/^[a-z\.\-0-9]+$/',
                'error' => 'Request param "site" is not valid set.',
            ],

            'offer_type' => [
                'from' => 'GET',
                'type' => 'iteger',
                'default' => null,
                'regexp' => '/^[a-z\.\-0-9]+$/',
                'error' => 'Request param "offer_type" is not valid set.',
            ],

        ],

        'getMyFavoritesAction' => [
            '__isRequest' => false,

            /* GET */
            'guid' => [
                'from' => 'GET',
                'type' => 'string',
                'regexp' => '/^[a-z0-9]+$/',
                'error' => 'Request param "guid" is not valid set.',
            ],

            'domain' => [
                'from' => 'GET',
                'type' => 'string',
                'default' => "",
                'regexp' => '/^[a-z\.\-0-9]+$/',
                'error' => 'Request param "domain" is not valid set.',
            ],

            'offer_type' => [
                'from' => 'GET',
                'type' => 'integer',
                'default' => null,
                'regexp' => '/^[0-9]+$/',
                'error' => 'Request param "offer_type" is not valid set.',
            ],
        ],
    ];

    public function indexAction() {
    }

    public function __construct($params = []) {
        $this->repository = $this
            ->getServiceManager()
            ->getRepository('Item_Favorite');

        parent::__construct($params);
    }

    public function getRepository() {
        return $this->repository;
    }
/////////////////////////////////////////////////////////////////////////////////////////////
    public function addFavoritesAction ($guid, $offer_id, $domain = null, $offer_type = null) {

        return $this
            ->getRepository()
            ->addFavorites($guid, $offer_id, $domain, $offer_type);
    }

    public function removeFavoritesAction ($guid, $offer_id, $domain = null, $offer_type = null) {

        return $this
            ->getRepository()
            ->removeFavorites($guid, $offer_id, $domain);
    }

    public function isFavoritesAction ($guid, $offer_id, $domain = null, $offer_type = null) {

        return $this
            ->getRepository()
            ->isFavorites($guid, $offer_id, $domain, $offer_type);
    }

    public function addFavoritesFromSessionAction($guid, $offers, $domain = null, $offer_type = null) {

        return $this
            ->getRepository()
            ->addFavoritesFromSession($guid, $offers, $domain, $offer_type);
    }

    public function getMyFavoritesAction($guid, $domain, $offer_type) {

        return $this
            ->getRepository()
            ->getMyFavorites($guid, $domain, $offer_type);

    }
}
