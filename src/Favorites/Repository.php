<?php

/**
 * Microservice
 *
 * @package    Favorites/Repository
 * @version    1.0
 */
use Zend\Db\Sql\Sql;

class Favorites_Repository extends Db_Zend_Repository {

    protected static $is_transact = false;

    public function isFavorites($user_id = 0, $offer_id = 0, $domain = null, $offer_type = null) {

        if(!$offer_id||!$user_id) {
            return false;
        }

        $sql = new Sql($this->adapter);

        $select = $sql
            ->select()
            ->from($this->getTable())
            ->where([
                'offer_id' => $offer_id,
                'guid' => $user_id
            ]);

        if($domain) {
            $select->where(['domain' => $domain]);
        }

        if($offer_type) {
            $select->where(['offer_type' => $offer_type]);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $row = $results->current();

        return $row? true: false;
    }

    public function removeFavorites($user_id = 0, $offer_id = 0, $domain = null, $offer_type = null) {

        if(!$offer_id||!$user_id) {
            return false;
        }

        $sql = new Sql($this->adapter);

        $select = $sql
            ->delete()
            ->from($this->getTable())
            ->where([
                'offer_id' => $offer_id,
                'guid' => $user_id
            ]);

        if($domain) {
            $select->where(['domain' => $domain]);
        }

        if($offer_type) {
            $select->where(['offer_type' => $offer_type]);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result->getAffectedRows();
    }


    public function addFavorites($user_id = 0, $offer_id = 0, $domain = null, $offer_type = null) {

        if(!$offer_id||!$user_id) {
            return false;
        }

        $sql = new Sql($this->adapter);

        $insert = $sql
            ->insert()
            ->into($this->getTable())
            ->columns(['offer_id', 'guid', 'domain', 'offer_type'])
            ->values([$offer_id, $user_id, $domain, $offer_type]);

        $statement = $sql->prepareStatementForSqlObject($insert);
        $result = $statement->execute();

        return $result->getAffectedRows();
    }

    public function getMyFavorites($user_id = 0, $domain = null, $offer_type = null) {

        if(!$user_id){
            return [];
        }

        $sql = new Sql($this->adapter);

        $select = $sql
            ->select()
            ->from($this->getTable())
            ->where([
                'guid' => $user_id
            ]);
        /*
        if($domain) {
            $select->where(['domain' => $domain]);
        }
        */
        if($offer_type) {
            $select->where(['offer_type' => $offer_type]);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $result = [];

        while($row = $results->current()) {
            $result[] = $row;
            $results->next();
        }

        return $result? $result: [];
    }

    public function addFavoritesFromSession($user_id = 0, $offers = [], $domain = null, $offer_type = null){

        if(!$user_id||!count($offers)){
            return [];
        }
        $result = [];
        foreach ($offers as $offer){

            if(!$this->isFavorites($user_id, $offer, $domain, $offer_type)){
                $result[] = $this->addFavorites($user_id, $offer, $domain, $offer_type);
            }
        }
        return $result;
    }
}